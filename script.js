const button = document.querySelector('.btn-change-theme');
const main = document.querySelector('section');

main.className = localStorage.getItem('theme');

button.addEventListener('click', function() {
    if (localStorage.getItem('theme') === 'items') {
        main.className = 'light-theme';
        localStorage.setItem('theme', 'light-theme')
    }
    else {
        main.className = 'items';
        localStorage.setItem('theme', 'items')
    }
});
